package sample;

import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.File;

public class Controller {
    public javafx.scene.control.Button Button;
    MediaPlayer mediaPlayer;
    private Label time;
    Duration duration;
    Button fullScreenButton;
    Scene scene;
    Media media;
    double width;
    double height;
    MediaView mediaView;

    public void start(Stage primaryStage) {


        scene = setScene(this.width, this.height);

        primaryStage.setTitle("MediaPlayer");
        primaryStage.setScene(scene);

        primaryStage.show();
    }

    public Scene setScene(double width, double height) {
        this.height = height;
        this.width = width;
//Add your own path of the vidio that you want to play
        String path = "F:/MediaPl/src/mediapl/Tom and Jerry Beach YouTube.mp4";

        media = new Media(new File(path).toURI().toString());

        mediaPlayer = new MediaPlayer(media);
//AutoPlay set to false
        mediaPlayer.setAutoPlay(false);
        mediaView = new MediaView(mediaPlayer);

// DropShadow effect
        DropShadow dropshadow = new DropShadow();
        dropshadow.setOffsetY(5.0);
        dropshadow.setOffsetX(5.0);
        dropshadow.setColor(Color.WHITE);

        mediaView.setEffect(dropshadow);

        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(mediaView);
        //borderPane.setBottom(addToolBar());

        borderPane.setStyle("-fx-background-color: Black");
        scene = new Scene(borderPane, 600, 600);
        scene.setFill(Color.BLACK);
        return scene;
    }
    public void OnAction(ActionEvent actionEvent) {
        mediaPlayer.play();
    };
}
